package com.example.highimpact;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HighImpactApplication {

	public static void main(String[] args) {
		SpringApplication.run(HighImpactApplication.class, args);
	}

}
