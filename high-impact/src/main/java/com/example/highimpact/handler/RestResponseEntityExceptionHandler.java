package com.example.highimpact.handler;

import com.example.highimpact.dto.ApiErrorDTO;
import com.example.highimpact.dto.ValidationErrorDTO;
import com.example.highimpact.exception.WebRequestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


import java.util.ArrayList;
import java.util.List;

import static com.example.highimpact.auth.filter.utils.HttpHeaderHelper.getHttpHeaders;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.util.StringUtils.capitalize;

/**
 * Clase que controla como procesar las excepciones en la capa controller.
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
}
