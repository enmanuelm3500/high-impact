package com.example.highimpact.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Optional;

public class AtmFilter {

    @JsonProperty("city")
    private Optional<String> city;

    @JsonProperty("distance")
    private Optional<String> distance;

    @JsonProperty("houseNumber")
    private Optional<String> houseNumber;

    @JsonProperty("lat")
    private Optional<String> lat;

    @JsonProperty("lng")
    private Optional<String> lng;

    @JsonProperty("postalCode")
    private Optional<String> postalCode;


    @JsonProperty("street")
    private Optional<String> street;

    @JsonProperty("type")
    private Optional<String> type;

    public AtmFilter() {
    }

    public Optional<String> getCity() {
        return city;
    }

    public void setCity(Optional<String> city) {
        this.city = city;
    }

    public Optional<String> getDistance() {
        return distance;
    }

    public void setDistance(Optional<String> distance) {
        this.distance = distance;
    }

    public Optional<String> getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(Optional<String> houseNumber) {
        this.houseNumber = houseNumber;
    }

    public Optional<String> getLat() {
        return lat;
    }

    public void setLat(Optional<String> lat) {
        this.lat = lat;
    }

    public Optional<String> getLng() {
        return lng;
    }

    public void setLng(Optional<String> lng) {
        this.lng = lng;
    }

    public Optional<String> getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Optional<String> postalCode) {
        this.postalCode = postalCode;
    }

    public Optional<String> getStreet() {
        return street;
    }

    public void setStreet(Optional<String> street) {
        this.street = street;
    }

    public Optional<String> getType() {
        return type;
    }

    public void setType(Optional<String> type) {
        this.type = type;
    }
}
