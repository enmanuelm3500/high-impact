package com.example.highimpact.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Atm {

    @JsonProperty("address")
    private Address address;

    @JsonProperty("distance")
    private Integer distance;

    @JsonProperty("type")
    private String type;

    @JsonProperty(value = "cityAtm", required = false)
    private String cityAtm;

    @JsonProperty(value = "streetAtm", required = false)
    private String streetAtm;

    public Atm() {
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCityAtm() {
        return cityAtm;
    }

    public void setCityAtm(String cityAtm) {
        this.cityAtm = cityAtm;
    }

    public String getStreetAtm() {
        return streetAtm;
    }

    public void setStreetAtm(String streetAtm) {
        this.streetAtm = streetAtm;
    }
}
