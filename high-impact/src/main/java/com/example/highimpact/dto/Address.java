package com.example.highimpact.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Address {

    @JsonProperty("street")
    private String street;

    @JsonProperty("houseNumber")
    private String housenumber;

    @JsonProperty("postalCode")
    private String postalcode;

    @JsonProperty("city")
    private String city;

    @JsonProperty(value = "geoLocation")
    private GeoLocation geoLocation;

    public Address() {
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHousenumber() {
        return housenumber;
    }

    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }

    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }
}
