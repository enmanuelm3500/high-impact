package com.example.highimpact.controller;

import com.example.highimpact.dto.Atm;
import com.example.highimpact.dto.AtmFilter;
import com.example.highimpact.service.AtmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/atm")
public class ATMController {

    @Autowired
    private AtmService atmService;

    @PostMapping("/listWithFilter")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<Atm>> getListWithFilter(@RequestBody AtmFilter atmfilter) {
        return createOkStatusResponseEntity(atmService.getListWithFilter(atmfilter));
    }

    @GetMapping("/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<Atm>> getList() {
        return createOkStatusResponseEntity(atmService.getList());
    }

    protected ResponseEntity createOkStatusResponseEntity(Object body) {
        return buildResponseEntity(body, OK);
    }

    private ResponseEntity buildResponseEntity(Object body, HttpStatus status) {
        return new ResponseEntity<>(body, status);
    }

}
