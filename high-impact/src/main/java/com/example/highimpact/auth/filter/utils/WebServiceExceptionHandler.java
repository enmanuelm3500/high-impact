package com.example.highimpact.auth.filter.utils;

import com.example.highimpact.exception.RestApplicationException;
import com.example.highimpact.handler.RestResponseEntityExceptionHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;

/**
 * Esta clase de conveniencia ayuda a manejar las excepciones que puedan surgir al momento de validar un token o durante
 * la generación del mismo.
 * <p>
 * Lo ideal sería usar {@link RestResponseEntityExceptionHandler}, pero, los filters funcionan por encima de la capa de
 * controllers, por tal motivo {@link RestResponseEntityExceptionHandler} no es capaz de manejar estas excepciones y
 * transformarlas en {@link ResponseEntity}. Esta clase hace el trabajo de {@link RestResponseEntityExceptionHandler},
 * pero invocado desde los distintos filters.
 *
 * @since 0.4.0
 */
public class WebServiceExceptionHandler {

}
