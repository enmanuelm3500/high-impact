package com.example.highimpact.service.impl;

import com.example.highimpact.dto.Atm;
import com.example.highimpact.dto.AtmFilter;
import com.example.highimpact.service.AtmService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AtmServiceImpl implements AtmService {

    @Value("${url.atm.api}")
    private String urlApi;

    @Override
    public List<Atm> getList() {
        Atm[] convertToObject = new Gson().fromJson(consultingAPi().getBody(), Atm[].class);
        return setCitysAndStreet(Arrays.asList(convertToObject));
    }

    @Override
    public List<Atm> getListWithFilter(AtmFilter atmFilter) {
        Atm[] convertToObject = new Gson().fromJson(consultingAPi().getBody(), Atm[].class);
        return filters(setCitysAndStreet(Arrays.asList(convertToObject)), atmFilter);
    }


    private List<Atm> filters(List<Atm> atms, AtmFilter filter) {
        atms = filter.getType().isPresent() ? filterByType(atms, filter.getType().get()) : atms;
        atms = filter.getDistance().isPresent() ? filterByDistance(atms, filter.getDistance().get()) : atms;
        atms = filter.getStreet().isPresent() ? filterByStreet(atms, filter.getStreet().get()) : atms;
        atms = filter.getHouseNumber().isPresent() ? filterByHouseNumber(atms, filter.getHouseNumber().get()) : atms;
        atms = filter.getPostalCode().isPresent() ? filterByPostalCode(atms, filter.getPostalCode().get()) : atms;
        atms = filter.getCity().isPresent() ? filterByCity(atms, filter.getCity().get()) : atms;
        atms = filter.getLat().isPresent() ? filterByLat(atms, filter.getLat().get()) : atms;
        atms = filter.getLng().isPresent() ? filterByLng(atms, filter.getLng().get()) : atms;
        return atms;
    }

    private List<Atm> filterByLng(List<Atm> atms, String lng) {
        return atms.stream()
                .filter(atm -> atm.getAddress()
                        .getGeoLocation()
                        .getLng().toLowerCase().equals(lng.toLowerCase()))
                .collect(Collectors.toList());
    }

    private List<Atm> filterByLat(List<Atm> atms, String lat) {
        return atms.stream()
                .filter(atm -> atm.getAddress()
                        .getGeoLocation()
                        .getLat().toLowerCase().equals(lat.toLowerCase()))
                .collect(Collectors.toList());
    }

    private List<Atm> filterByCity(List<Atm> atms, String city) {
        return atms.stream()
                .filter(atm -> atm.getAddress()
                        .getCity()
                        .toLowerCase()
                        .equals(city.toLowerCase()))
                .collect(Collectors.toList());
    }

    private List<Atm> filterByPostalCode(List<Atm> atms, String postalcode) {
        return atms.stream()
                .filter(atm -> atm.getAddress()
                        .getPostalcode()
                        .toLowerCase()
                        .equals(postalcode.toLowerCase()))
                .collect(Collectors.toList());
    }

    private List<Atm> filterByHouseNumber(List<Atm> atms, String houseNumber) {
        return atms.stream()
                .filter(atm -> atm.getAddress()
                        .getHousenumber()
                        .toLowerCase()
                        .equals(houseNumber.toLowerCase()))
                .collect(Collectors.toList());
    }

    private List<Atm> filterByType(List<Atm> atms, String type) {
        return atms.stream()
                .filter(atm -> atm.getType()
                        .toLowerCase()
                        .equals(type.toLowerCase()))
                .collect(Collectors.toList());
    }

    private List<Atm> filterByDistance(List<Atm> atms, String distance) {
        return atms.stream()
                .filter(atm -> atm
                        .getDistance()
                        .equals(distance))
                .collect(Collectors.toList());
    }

    private List<Atm> filterByStreet(List<Atm> atms, String street) {
        return atms.stream()
                .filter(atm -> atm.getAddress()
                        .getStreet()
                        .toLowerCase()
                        .equals(street.toLowerCase()))
                .collect(Collectors.toList());
    }


    private List<Atm> setCitysAndStreet(List<Atm> atms) {
        return atms.stream().map(this::setCityAndStreet).collect(Collectors.toList());
    }

    private Atm setCityAndStreet(Atm atm) {
        atm.setStreetAtm(atm.getAddress().getStreet());
        atm.setCityAtm(atm.getAddress().getCity());
        return atm;
    }

    private ResponseEntity<String> consultingAPi() {
        RestTemplate template = new RestTemplate();
        return template.exchange(urlApi, HttpMethod.GET, generateHeader(), String.class);
    }

    private HttpEntity generateHeader() {
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity entity = new HttpEntity<>(requestHeaders);
        return entity;
    }


}
