package com.example.highimpact.service;

import com.example.highimpact.entity.Role;
import com.example.highimpact.entity.Users;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UsersDetailsAplication implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Users users = getUser();
        return buildUser(users);
    }

    private Users getUser() {
        Users user = new Users();
        user.setEnabled(true);
        user.setUsername("admin");
        user.setId(1L);
        user.setRoles(getRol());
        return user;
    }

    private List<Role> getRol() {
        List<Role> roles = new ArrayList<>();
        Role rol = new Role();
        rol.setAuthority("ROLE_ADMIN");
        roles.add(rol);
        return roles;
    }

    private User buildUser(Users users) {
        Set<GrantedAuthority> roles = buildAutority(users.getRoles());
        return new User(users.getUsername(), "$2a$10$O9wxmH/AeyZZzIS09Wp8YOEMvFnbRVJ8B4dmAMVSGloR62lj.yqXG", true, true, true, true, roles);
    }


    private Set<GrantedAuthority> buildAutority(List<Role> roles) {
        Set<GrantedAuthority> authorities = new HashSet<>();

        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
        }

        if (authorities.isEmpty()) {
            throw new UsernameNotFoundException("Error en el Login: usuario  no tiene roles asignados!");
        }
        return authorities;
    }
}
