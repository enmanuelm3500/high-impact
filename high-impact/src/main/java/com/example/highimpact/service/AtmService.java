package com.example.highimpact.service;

import com.example.highimpact.dto.Atm;
import com.example.highimpact.dto.AtmFilter;

import java.util.List;

public interface AtmService {

    List<Atm> getList();

    List<Atm> getListWithFilter(AtmFilter atmFilter);


}
