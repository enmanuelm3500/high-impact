package com.example.highimpact.entity;

import java.io.Serializable;


public class Role implements Serializable {


    private Long id;

    private String authority;

    public Role() {
    }

    public Role(String authority) {
        this.authority = authority;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

}
